export interface ShopEntity {
    id: number;
    name: string;
    description: string;
    rating: number;
    liked: number;
    bonus_min: number;
    bonus_max: number;
    image_path: string;
}
