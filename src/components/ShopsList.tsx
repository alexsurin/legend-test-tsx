import * as React from 'react'
import {ShopItem} from './ShopItem';
import {SortForm} from './SortForm';
import {ShopEntity} from '../models';

interface State {
    shops: ShopEntity[];
    sortType: string
}
interface Props {

}

export class ShopsList extends React.Component<Props, State> {
    constructor(props:Props) {
        super(props);

        this.state = {
            shops: [],
            sortType: 'id'
        };

        this.updateData =  this.updateData.bind(this);
    }

    componentDidMount() {
        this.updateData();
    }

    updateData(sortType:any = null) {

        if (sortType) {
            var sortString:string = '?_sort='+sortType+'&_order=desc';
        } else {
            var sortString:string = '?_sort='+this.state.sortType+'&_order=desc';
        }

        fetch('http://localhost:3000/shops'+sortString)
            .then(response => response.json())
            .then(
                (result) => {
                    this.setState({
                        shops: result,
                        sortType: sortType ? sortType : this.state.sortType
                    });
                }
            );
    }

    render() {
        return (
            <div className="ShopsList">
                <div className="bunner">
                    <a href="#"><img src="img/bunner.png"/></a>
                </div>
                <div className="content_bar">
                    <div className="content_bar__title">
                        <h2>Заведения</h2>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><a href="#">Home</a></li>
                                <li className="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                    <SortForm updateData={this.updateData}/>
                </div>
                <div className="shop">
                    <div className="container_fluid">
                        <div id="root" className="row">
                            {this.state.shops.map((shop) => {
                                return <ShopItem key={shop.id} shop={shop} updateData={this.updateData}/>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
