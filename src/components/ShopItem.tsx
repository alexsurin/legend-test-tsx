import * as React from 'react'
import {ShopEntity} from "../models";

interface Props {
    shop: ShopEntity;
    updateData: any;
}

export class ShopItem extends React.Component<Props> {

    like(event:React.MouseEvent) {
        event.preventDefault();

        var like:string = this.props.shop.liked == 1 ? '0' : '1';

        fetch('http://localhost:3000/shops/'+this.props.shop.id,
            {
                method: 'PATCH',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    liked: like,
                })
            })
            .then(response => response.json())
            .then(
                (result) => {
                    this.setState({
                        shops: result
                    });
                    this.props.updateData();
                }
            );
    }

    render() {

        var likeClass:string = this.props.shop.liked == 1 ? 'like like_active':'like';

        return (
            <div className="ShopItem col-xl-4">
                <a href="shop_full.html" className="shop_item">
                    <div className="shop_item__title">
                        <h3>{this.props.shop.name}</h3>
                        <div className="shop_item__title___right">
                            <button className={likeClass} onClick={(event) => {this.like(event)}}>
                                <i className="fas fa-heart"></i>
                            </button>
                        </div>
                    </div>
                    <div className="shop_item__center">
                        <div className="shop_item__center___avatar">
                            <div className="shop_avatar">
                                <img src={this.props.shop.image_path} />
                                <span>{this.props.shop.rating}</span>
                            </div>
                        </div>
                        <div className="shop_item__center___text">
                            <p>{this.props.shop.description}</p>
                        </div>
                    </div>
                    <div className="shop_item__bonus">
                        <h3>{this.props.shop.bonus_min} - {this.props.shop.bonus_max}<small>%</small></h3>
                        <span>Бонусов</span>
                    </div>
                </a>
            </div>
        );
    }
}
