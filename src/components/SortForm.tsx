import * as React from 'react'

interface Props {
    updateData: any;
}

export class SortForm extends React.Component<Props> {

    sort(event:React.FormEvent<HTMLSelectElement>) {
        this.props.updateData(event.currentTarget.value);
    }

    render() {
        return (
            <div className="SortForm content_bar__sorting">
                Сортировка:
                <select onChange={(event) => {this.sort(event)}}>
                    <option value={'id'}>Новые</option>
                    <option value={'liked'}>Избраные</option>
                </select>
            </div>
        );

    }
}
